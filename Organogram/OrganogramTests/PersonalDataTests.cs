﻿using Organogram;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace OrganogramTests
{
    [TestClass]
    public class PersonalDataTests 
    {
        [TestMethod]
        public void SetReferencesBetweenPeopleTest()
        {
            //Arrange
            List<Person> pData = new List<Person>() {
                new Person(new string[] { "1", "0", "", "", "", "", "", "", "" }),
                new Person(new string[] { "2", "1", "", "", "", "", "", "", "" }),
                new Person(new string[] { "3", "1", "", "", "", "", "", "", "" }),
                new Person(new string[] { "4", "2", "", "", "", "", "", "", "" }),
                new Person(new string[] { "5", "3", "", "", "", "", "", "", "" }),
                new Person(new string[] { "6", "3", "", "", "", "", "", "", "" }),
                new Person(new string[] { "7", "5", "", "", "", "", "", "", "" })};
            //Act
            PersonalData.SetReferencesBetweenPeople(pData);

            //Assert
            Assert.AreEqual(4, pData[0].Subordinates[0].Subordinates[0].Id);
            Assert.AreEqual(7, pData[0].Subordinates[1].Subordinates[0].Subordinates[0].Id);
            Assert.AreEqual(6, pData[0].Subordinates[1].Subordinates[1].Id);

        }


        [TestMethod]
        public void OrganizingPersonalDataTest()
        {
            //Arrange
            string[] lines = {
            "8,7,Andrzej,Wajda,Microsoft,Johannesburg,IT Manager,0765421456,0115425230,0115425290",
            "17,3,Karol,Krawczyk,Oracle,Johannesburg,Software Developer,0726594521,0113589799,0113589798",
            "1,0,Anna,Jantar,IBM,Johannesburg,Managing Director,0762349827,0116725362,0116725368",
            "3,3,Mahadma,Ghandi,IBM,Johannesburg,Technical Director,0843546837,0116725321,0116725322",
            "18,3,Karl,Young,Oracle,Johannesburg,Senior Analyst,0826587441,0113589799,0113589798"};
            
            //Act
            List<Person> people = PersonalData.OrganizingPersonalData(lines);

            //Assert
            Assert.AreEqual(8, people[0].Id);
            Assert.AreEqual(3, people[1].SuperiorId);
            Assert.AreEqual("Anna", people[2].Name);
            Assert.AreEqual("Ghandi", people[3].Surname);
            Assert.AreEqual("Senior Analyst", people[4].JobTitle);

        }


        [TestMethod]
        public void GetHighestInHierarchyTest()
        {
            Person _1 = new Person(new string[] { "1", "0", "", "", "", "", "", "", "" });
            Person _2 = new Person(new string[] { "2", "1", "", "", "", "", "", "", "" });
            Person _3 = new Person(new string[] { "3", "1", "", "", "", "", "", "", "" });
            Person _4 = new Person(new string[] { "4", "2", "", "", "", "", "", "", "" });
            Person _5 = new Person(new string[] { "5", "3", "", "", "", "", "", "", "" });
            Person _6 = new Person(new string[] { "6", "3", "", "", "", "", "", "", "" });
            Person _7 = new Person(new string[] { "7", "5", "", "", "", "", "", "", "" });
            List<Person> personalData = new List<Person>() {_1, _2, _3,  _4, _5, _6, _7 };
            _1.Subordinates.Add(_3);
            _1.Subordinates.Add(_2);
            _2.Superior = _1;
            _3.Superior = _1;

            _2.Subordinates.Add(_4);
            _4.Superior = _2;

            _3.Subordinates.Add(_5);
            _3.Subordinates.Add(_6);
            _5.Superior = _3;
            _6.Superior = _3;

            _5.Subordinates.Add(_7);
            _7.Superior = _5;


            //Act
            var highestInHierarchy = PersonalData.GetHighestInHierarchy(personalData);

            //Assert
            Assert.AreEqual(1, highestInHierarchy[0].Id);
        }
    }
}
