﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organogram
{
    public class Person
    {
        public int Id { get; set;  }
        public int SuperiorId { get; set; }
        public string Name { get; }
        public string Surname { get; }
        public string Company { get; }
        public string City { get; }
        public string JobTitle { get; }
        public string[] Numbers { get; }
        public Person Superior { get; set;  }
        public List<Person> Subordinates { get; set; }


        public Person(string[] rawPersonData)
        {
            Id = int.Parse(rawPersonData[0]);
            SuperiorId = int.Parse(rawPersonData[1]);
            Name = rawPersonData[2];
            Surname = rawPersonData[3];
            Company = rawPersonData[4];
            City = rawPersonData[5];
            JobTitle = rawPersonData[6];
            Numbers = rawPersonData.Skip(7).Take(rawPersonData.Length - 7).ToArray();
            Subordinates = new List<Person>();
        }


        public string GetInfo()
        {
            return $"{Name} {Surname}, {Company}, {JobTitle}";
        }
    }
}
