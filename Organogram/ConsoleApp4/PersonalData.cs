﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organogram
{
    public class PersonalData
    {
        public static string directoryPath = Directory.GetCurrentDirectory().Replace("\\bin\\Debug", "");
        public static string fileName = "companies_data.csv";
        public static string fullFilePath = Path.Combine(directoryPath, fileName);


        public static void PrintOrganogram(List<Person> personalData, int depth = 0)
        {
            depth++;
            foreach (Person p in personalData)
            {

                string personInfo = p.GetInfo();

                if (depth == 1)
                {
                    Console.WriteLine(personInfo);
                }
                else
                {
                    for (int i = 0; i < depth; i++)
                    {
                        Console.Write("  ");
                    }
                    Console.Write("--> ");
                    Console.WriteLine(personInfo);
                }
                List<Person> sortedSubordinates = p.Subordinates.OrderBy(i => i.Id).ToList();
                PrintOrganogram(sortedSubordinates, depth);
            }
        }


        public static List<Person> PrepareData()
        {
            string[] rawPeopleData = File.ReadAllLines(fullFilePath);

            List<Person> peopleData = OrganizingPersonalData(rawPeopleData);
            SetReferencesBetweenPeople(peopleData);

            List<Person> highestInHierarchy = GetHighestInHierarchy(peopleData);

            return highestInHierarchy;
        }


        public static List<Person> OrganizingPersonalData(string[] rawPeopleData)
        {
            List<Person> peopleList = new List<Person>();
            

            foreach (string line in rawPeopleData)
            {
                string[] rawPersonData = line.Split(',');
                Person newPerson = new Person(rawPersonData);
                peopleList.Add(newPerson);
            }

            return peopleList;
        }


        public static void SetReferencesBetweenPeople(List<Person> personalData)
        {
            foreach (Person person in personalData)
            {
                Person superior = personalData.Where(
                    p => p.Id == person.SuperiorId)
                    .FirstOrDefault();

                if (superior != null)
                {
                    superior.Subordinates.Add(person);
                    person.Superior = superior;
                }
            }
        }


        public static List<Person> GetHighestInHierarchy(List<Person> personalData)
            => personalData.Where(p => p.Superior == null).OrderBy(p => p.Id).ToList();
    }
}
