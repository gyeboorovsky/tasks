﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Organogram
{
    class Program
    {
        static void Main()
        {
            List<Person> peopleData = PersonalData.PrepareData();
            PersonalData.PrintOrganogram(peopleData);

            Console.ReadKey();
        }
    }
}
